﻿using FlightManagementSystem.ViewModels.BaseViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace FlightManagementSystem.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private string response = "Hello";
        public string Response
        {
            get
            {
                return response;
            }
            set
            {
                response = value;
                OnPropertyChanged("Response");
            }
        }

        private ICommand submitCommand; 
        public ICommand SubmitCommand
        {
            get { return submitCommand = submitCommand ?? new RelayCommand(param => Submit(), null); }
        }

        private void Submit()
        {
            Response = "Heyyy";
        }

    }
}
