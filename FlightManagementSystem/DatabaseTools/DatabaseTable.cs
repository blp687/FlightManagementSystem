﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightManagementSystem.DatabaseTools
{
    public class DatabaseTable
    {
        MySqlConnection connection;
        MySqlDataAdapter adapter;
        public DataTable GetTable(String query, String sortBy)
        {
            String connString = "server=localhost;uid=root;pwd=xxxx;database=comics;";
            connection = new MySqlConnection(connString);
            adapter = new MySqlDataAdapter(query, connection);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            dataTable.DefaultView.Sort = sortBy;
            return dataTable;
        }
    }
}
